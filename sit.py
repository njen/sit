"""
Generate Special Information Tones

https://en.wikipedia.org/wiki/Special_information_tone

```
import sit

for i in sit.tones.values():
    i.save_wav()
```
"""


# Imports


import array
import struct
import wave

from math import pi
from math import sin


# Constants


SAMPLE_RATE = 44100.0 # s**-1

SHORT = 276.0 # ms
LONG = 380.0 # ms
PAUSE = 2.0 # ms

LOW_1 = 913.8 # Hz
HIGH_1 = 985.2 # Hz
LOW_2 = 1370.6 # Hz
HIGH_2 = 1428.5 # Hz
LOW_3 = 1776.7 # Hz
SILENCE = 0.0 # Hz


# Derived

def wav(freq, duration=LONG, volume=50):
    """
    :param freq: float (Hz)
    :param duration: float (ms)
    :param volume: int/float (%)
    :return:  
    """
    data = []
    tpf = 2 * pi * freq
    vol = volume / 100 * 32767
    samples = int(SAMPLE_RATE * duration / 1000)
    for i in range(samples):
        value = int(vol * sin(tpf * i / SAMPLE_RATE))
        data.append(struct.pack('<h', value))
    return data


long_high_1 = wav(HIGH_1)
long_high_2 = wav(HIGH_2)
long_low_1 = wav(LOW_1)
long_low_2 = wav(LOW_2)
long_low_3 = wav(LOW_3)

short_high_1 = wav(HIGH_1, SHORT)
short_high_2 = wav(HIGH_2, SHORT)
short_low_1 = wav(LOW_1, SHORT)
short_low_2 = wav(LOW_2, SHORT)

pause_1 = wav(SILENCE, 100)      # leading
pause_12 = wav(SILENCE, PAUSE)   # between first and second
pause_23 = pause_12              # between second and third
pause_3 = wav(SILENCE, 100)      # trailing

pauses = (pause_1, pause_12, pause_23, pause_3)


def tone(notes=tuple(), pauses=pauses):
    data = []
    for i in range(len(notes)):
        data.extend(pauses[i])
        data.extend(notes[i])
    data.extend(pauses[i + 1])
    return data


class Tone:

    def __init__(self, code, name, description, tone, fn):
        self.code = code
        self.name = name
        self.description = description
        self.tone = tone
        self.fn = fn

    def save_wav(self):
        w = wave.open(f'{self.fn}.wav', 'w')
        w.setnchannels(1)
        w.setsampwidth(2)
        w.setframerate(SAMPLE_RATE)
        for data in self.tone:
            w.writeframesraw(data)
        w.close()


tones = {
    "RO'": Tone(
        code="RO'",
        name='Reorder - intraLATA',
        description=' '.join((
            'Incomplete digits, internal office or feature failue -',
            'local office',
        )),
        tone=tone((short_low_1, long_high_2, long_low_3)),
        fn='ro1',
    ),
    'VC': Tone(
        code='VC',
        name='Vacant Code',
        description='Unassigned N11 code, CLASS code or prefix',
        tone=tone((long_high_1, short_low_2, long_low_3)),
        fn='vc',
    ),
    "NC'": Tone(
        code="NC'",
        name='No Circuit - intraLATA',
        description='All circuits busy - local office',
        tone=tone((long_high_1, long_high_2, long_low_3)),
        fn='nc1',
    ),
    'IC': Tone(
        code='IC',
        name='Intercept',
        description='Number changed or disconnected',
        tone=tone((short_low_1, short_low_2, long_low_3)),
        fn='ic',
    ),
    "RO''": Tone(
        code="RO''",
        name="Reorder - interLATA",
        description=' '.join((
            'Call failure, no wink or partial digits received -',
            'distant office',
        )),
        tone=tone((short_high_1, long_low_2, long_low_3)),
        fn='ro2',
    ),
    "NC''": Tone(
        code="NC''",
        name='No Circuit - interLATA',
        description='All circuits busy - distant office',
        tone=tone((long_low_1, long_low_2, long_low_3)),
        fn='nc2',
    ),
    'IO': Tone(
        code='IO',
        name='Ineffective/Other',
        description=' '.join((
            'General misdialing, coin deposit required',
            'or other failure',
        )),
        tone=tone((long_low_1, long_low_2, long_low_3)),
        fn='io',
    ),
    '-': Tone(
        code='-',
        name='Future Use',
        description='Reserved for future use',
        tone=tone((short_high_1, short_high_2, long_low_3)),
        fn='future',
    ),
}


if __name__ == '__main__':
    for i in tones.values():
        i.save_wav()


